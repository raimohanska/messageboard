# MessageBoard sample application

## Build and run

Requires Java and Maven. Build like this:

    mvn install

.. and you'll get `target/messageboard.war` that you can deploy on Jetty or Tomcat.

You can run the application locally:

    mvn jetty:run

## Test

When you run `mvn install` the automatica tests are also run. Pls see the `MessageResourceTest` class for details.

## Try it with cURL

Post new message

    curl -v -H "content-type:application/json" -d '{"sender": "juha", "title": "hello world", "content": "welcome to msgbrd", "url": "http://google.com"}' localhost:8080/messages

Get messages as JSON

    curl http://localhost:8080/messages -H "accept: application/json"

Get messages as XML

    curl http://localhost:8080/messages -H "accept: application/xml"

Get messages using simple message format (just sender, title, content)

    curl http://localhost:8080/messages?format=simple