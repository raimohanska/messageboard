package com.messageboard;

import static org.apache.commons.lang3.StringUtils.trimToEmpty;

import com.messageboard.domain.Message;
import com.messageboard.domain.SimpleMessage;

/**
 *  Message formatter abstraction with `getFormatterByType` helper for finding a formatter.
 *
 *  A MessageFormatter is used to include either all Message fields or a subset.
 *
 *  Implementation note: arrays are used instead of Lists to make JAXB serialization into XML
 *  work without any extra configuration. Lists would allow for a nicer implementation otherwise.
 */
abstract class MessageFormatter {
    public static MessageFormatter getFormatterByType(String format) {
        switch (trimToEmpty(format)) {
            case "simple": return new SimpleMessageFormatter();
            default: return new DefaultMessageFormatter();
        }
    }
    public abstract Object formatMessages(Message[] messages);
}

class DefaultMessageFormatter extends MessageFormatter {
    @Override
    public Object formatMessages(final Message[] messages) {
        return messages;
    }
}

class SimpleMessageFormatter extends MessageFormatter {
    @Override
    public Object formatMessages(final Message[] messages) {
        final SimpleMessage[] simpleMessages = new SimpleMessage[messages.length];
        for (int i = 0; i < messages.length; i++) {
            Message message = messages[i];
            simpleMessages[i] = new SimpleMessage(message.title, message.content, message.sender);
        }
        return simpleMessages;
    }
}
