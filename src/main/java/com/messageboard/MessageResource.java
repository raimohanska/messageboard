package com.messageboard;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.LoggerFactory;

import com.messageboard.database.InMemoryMessageDatabase;
import com.messageboard.database.MessageDatabase;
import com.messageboard.domain.Message;

@Path("messages")
public class MessageResource {
    private MessageDatabase db = InMemoryMessageDatabase.INSTANCE;
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(MessageResource.class);

    /**
     * Get list of all messages. The options query param `format=simple` can be used to output a
     * simplified version. The `accept` HTTP header can be used to select the suitable format
     * (either application/xml or application/json).
     *
     * Example: curl http://localhost:8080/messages -H "accept: application/json"
     * Example2: curl http://localhost:8080/messages?format=simple
     */
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response listAllMessages(@QueryParam("format") String format) {
        return Response.ok(MessageFormatter.getFormatterByType(format).formatMessages(db.listMessages())).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postMessage(Message message) {
        logger.info("Storing new message: " + message.content);
        db.postMessage(message);
    }
}

