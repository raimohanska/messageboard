package com.messageboard.database;

import java.util.ArrayList;
import java.util.List;

import com.messageboard.domain.Message;

/**
 *  In-memory message database implementation for prototyping / testing. Can be replaced with
 *  a real database. Could employ Guice to bind different DB impl for different envs.
 */
public class InMemoryMessageDatabase implements MessageDatabase {
    public final static InMemoryMessageDatabase INSTANCE = new InMemoryMessageDatabase();

    // List is treated as immutable
    private List<Message> messages;

    {
        clear();
        postMessage(new Message("hello world", "welcome to msgboard", "juha", "http://www.google.com"));
    }

    public synchronized Message[] listMessages() {
        return messages.toArray(new Message[messages.size()]);
    }

    @Override
    public synchronized void postMessage(final Message message) {
        messages = new ArrayList<>(messages);
        messages.add(message);
    }

    public synchronized void clear() {
        messages = new ArrayList<>();
    }
}
