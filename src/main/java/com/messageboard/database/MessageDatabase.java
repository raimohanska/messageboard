package com.messageboard.database;

import com.messageboard.domain.Message;

public interface MessageDatabase {
    Message[] listMessages();

    void postMessage(Message message);
}
