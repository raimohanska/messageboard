package com.messageboard.domain;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message extends DomainObject {
    public final String title;
    public final String content;
    public final String sender;
    public final String url;

    private Message() {
        this(null, null, null, null);
    }

    public Message(final String title, final String content, final String sender, final String url) {
        this.title = title;
        this.content = content;
        this.sender = sender;
        this.url = url;
    }
}
