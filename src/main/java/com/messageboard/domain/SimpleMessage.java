package com.messageboard.domain;

/**
 * Simple version of Message for mobile clients and such
 */
public class SimpleMessage extends DomainObject {
    public final String title;
    public final String content;
    public final String sender;

    private SimpleMessage() {
        this(null, null, null);
    }

    public SimpleMessage(final String title, final String content, final String sender) {
        this.title = title;
        this.content = content;
        this.sender = sender;
    }
}
