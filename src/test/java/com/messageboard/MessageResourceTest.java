package com.messageboard;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;

import com.messageboard.database.InMemoryMessageDatabase;
import com.messageboard.domain.Message;

public class MessageResourceTest extends JerseyTest {
    final Message exampleMessage = new Message("hello world", "welcome to msgboard", "juha", "http://www.google.com");
    final Message exampleMessageSimpleFormat = new Message("hello world", "welcome to msgboard", "juha", null);

    @Override
    protected Application configure() {
        return new ResourceConfig(MessageResource.class);
    }

    @Test
    public void defaultFormatJson() {
        final List<Message> messages = target().path("messages")
            .request().accept(MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Message>>() {});
        assertEquals(Arrays.asList(exampleMessage), messages);
    }

    @Test
    public void defaultFormatXML() {
        final List<Message> messages = target().path("messages")
            .request().accept(MediaType.APPLICATION_XML)
            .get(new GenericType<List<Message>>() {});
        assertEquals(Arrays.asList(exampleMessage), messages);
    }

    @Test
    public void simpleFormatJson() {
        final List<Message> messages = target().path("messages")
            .queryParam("format", "simple")
            .request().accept(MediaType.APPLICATION_JSON)
            .get(new GenericType<List<Message>>() {});
        assertEquals(Arrays.asList(exampleMessageSimpleFormat), messages);
    }

    @Before
    public void postMessage() {
        InMemoryMessageDatabase.INSTANCE.clear();
        target().path("messages").request().post(Entity.entity(exampleMessage, MediaType.APPLICATION_JSON_TYPE));
    }
}
